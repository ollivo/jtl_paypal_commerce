<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\PPC\Order;

use InvalidArgumentException;
use Plugin\jtl_paypal_commerce\PPC\PPCHelper;
use Plugin\jtl_paypal_commerce\PPC\Request\Serializer\JSON;
use Plugin\jtl_paypal_commerce\PPC\Request\Serializer\SerializerInterface;

/**
 * Class Shipping
 * @package Plugin\jtl_paypal_commerce\PPC\Order
 */
class Shipping extends JSON
{
    public const TYPE_SHIPPING = 'SHIPPING';
    public const TYPE_PICKUP   = 'PICKUP_IN_PERSON';

    /**
     * Shipping constructor.
     * @param object|null $data
     */
    public function __construct(?object $data = null)
    {
        parent::__construct($data ?? (object)[
            'type' => self::TYPE_SHIPPING,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function setData($data)
    {
        parent::setData($data);

        $adressData = $this->getData()->address ?? null;
        if ($adressData !== null && !($adressData instanceof Address)) {
            $this->setAddress(new Address($adressData));
        }

        return $this;
    }

    /**
     * @return Address
     */
    public function getAddress(): Address
    {
        return $this->data->address;
    }

    /**
     * @param Address $address
     * @return Shipping
     */
    public function setAddress(Address $address): self
    {
        $this->data->address = $address;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->data->name->full_name;
    }

    /**
     * @param string $name
     * @return Shipping
     */
    public function setName(string $name): self
    {
        $this->data->name = (object)[
            'full_name' => PPCHelper::shortenStr($name, 300),
        ];

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->data->type ?? self::TYPE_SHIPPING;
    }

    /**
     * @param string $type
     * @return Shipping
     */
    public function setType(string $type): self
    {
        if (!\in_array($type, [self::TYPE_SHIPPING, self::TYPE_PICKUP])) {
            throw new InvalidArgumentException(\sprintf('%s is not a valid shipping type.', $type));
        }

        $this->data->type = $type;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function isEmpty(): bool
    {
        return empty($this->getName()) && $this->getAddress()->isEmpty();
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        $data = clone $this->getData();

        if (empty($data->address) || ($data->address instanceof SerializerInterface && $data->address->isEmpty())) {
            unset($data->address);
        }

        return $data;
    }
}
