<?php /** @noinspection PhpMultipleClassDeclarationsInspection */
declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\PPC\Request;

use JsonException;
use function Functional\first;

/**
 * Class ClientErrorResponse
 * @package Plugin\jtl_paypal_commerce\PPC\Request
 */
class ClientErrorResponse extends JSONResponse
{
    /**
     * @inheritDoc
     */
    public function getExpectedResponseCode(): array
    {
        // current status code is the expected
        return [$this->getStatusCode()];
    }

    /**
     * @return string
     */
    public function getDebugId(): string
    {
        try {
            $data = $this->getData();
        } catch (JsonException | UnexpectedResponseException $e) {
            return '';
        }

        return $data->debug_id ?? '';
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        try {
            $data = $this->getData();
        } catch (JsonException | UnexpectedResponseException $e) {
            return 'Unknown client error';
        }

        return $data->name ?? 'Unknown client error';
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        try {
            $data = $this->getData();
        } catch (JsonException | UnexpectedResponseException $e) {
            return 'Unspecified error message';
        }

        return $data->message ?? 'Unspecified error message';
    }

    /**
     * @param int $pos
     * @return object|null
     */
    public function getDetail(int $pos = 0): ?ClientErrorDetail
    {
        try {
            $data = $this->getData();
        } catch (JsonException | UnexpectedResponseException $e) {
            return null;
        }

        return new ClientErrorDetail($data->details[$pos] ?? null);
    }

    /**
     * @param string $rel
     * @return string|null
     */
    public function getLink(string $rel): ?string
    {
        try {
            $data = $this->getData();
        } catch (JsonException | UnexpectedResponseException $e) {
            return null;
        }
        $link = first($data->links, static function (object $item) use ($rel) {
            return $item->rel === $rel;
        });

        return $link !== null ? $link->href : null;
    }
}
