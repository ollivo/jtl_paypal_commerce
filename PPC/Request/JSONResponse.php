<?php /** @noinspection PhpMultipleClassDeclarationsInspection */
declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\PPC\Request;

use JsonException;

/**
 * Class JSONResponse
 * @package Plugin\jtl_paypal_commerce\PPC\Request
 */
class JSONResponse extends PPCResponse
{
    /** @var string|null */
    private $content;

    /** @var bool */
    private $contentLoaded = false;

    /** @var int[] */
    protected $responseCodes = [200];

    /**
     * @return string|null
     * @throws UnexpectedResponseException
     */
    protected function getContent(): ?string
    {
        if (!\in_array($this->getStatusCode(), $this->getExpectedResponseCode(), true)) {
            throw new UnexpectedResponseException($this, $this->getExpectedResponseCode());
        }
        if (!$this->contentLoaded) {
            $body = $this->getBody();

            $this->content       = $body !== null ? $body->getContents() : null;
            $this->contentLoaded = true;
        }

        return \is_string($this->content) ? $this->content : null;
    }

    /**
     * @return mixed
     * @throws JsonException|UnexpectedResponseException
     */
    public function getData()
    {
        $content = $this->getContent();

        return $content !== null ? \json_decode($content, false, 512, \JSON_THROW_ON_ERROR) : null;
    }

    /**
     * @return int[]
     */
    public function getExpectedResponseCode(): array
    {
        return $this->responseCodes;
    }

    /**
     * @param int[] $responseCodes
     * @return static
     * @noinspection PhpMissingReturnTypeInspection
     */
    public function setExpectedResponseCode(array $responseCodes)
    {
        $this->responseCodes = $responseCodes;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        try {
            return $this->getContent() ?? '';
        } catch (UnexpectedResponseException $e) {
            return $e->getMessage();
        }
    }
}
