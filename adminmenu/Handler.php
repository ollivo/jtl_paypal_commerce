<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\adminmenu;

use JTL\Backend\NotificationEntry;
use JTL\DB\DbInterface;
use JTL\IO\IOResponse;
use JTL\Plugin\PluginInterface;
use JTL\Shop;

/**
 * Class Handler
 * @package Plugin\jtl_paypal_commerce\adminmenu
 */
class Handler
{
    /** @var PluginInterface */
    private $plugin;

    /** @var DbInterface */
    private $db;

    /**
     * Handler constructor.
     * @param PluginInterface  $plugin
     * @param DbInterface|null $db
     */
    public function __construct(PluginInterface $plugin, ?DbInterface $db = null)
    {
        $this->plugin = $plugin;
        $this->db     = $db ?? Shop::Container()->getDB();
    }

    /**
     * @return void
     */
    public function smarty(): void
    {
    }

    /**
     * @param string $tplElement
     * @return IOResponse
     * @noinspection PhpUnused
     */
    public function handleAjax(string $tplElement): IOResponse
    {
        $results             = [
            NotificationEntry::TYPE_DANGER  => '<i class="fa fa-times text-danger"></i>',  // red cross
            NotificationEntry::TYPE_WARNING => '<i class="fa fa-check text-warning"></i>', // orange hook
            NotificationEntry::TYPE_NONE    => '<i class="fa fa-check text-success"></i>', // green hook
        ];
        $resultWrap          = '<span data-html="true" data-toggle="tooltip" data-placement="left"
                title="" data-original-title="%s">%s</span>';
        $displayResult       = $results[NotificationEntry::TYPE_DANGER];
        [$paymentID, $tplID] = \explode('_', $tplElement);
        $infoCheck           = new TabInfoChecks($this->plugin);

        if (($tplID === 'payment-linked')) {
            $displayResult = $infoCheck->isShippmentLinked((int)$paymentID, $results, $resultWrap);
        }
        if (($tplID === 'ppc-connectable')) {
            $displayResult = $infoCheck->getConnectionInfo((int)$paymentID, $results, $resultWrap);
        }
        $response = new IOResponse();
        $response->assignDom(
            $paymentID.'_'.$tplID,
            'innerHTML',
            $displayResult
        );

        return $response;
    }
}
