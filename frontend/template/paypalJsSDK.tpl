<script src="{$ppcFrontendUrl}js/paypal.browser.min.js"></script>
<script>
        var ppcClientID          = '{$ppcClientID}',
            buttonActions        = null,
            ppcOrderLocale       = '{$ppcOrderLocale}',
            ppcCurrency          = '{$ppcCurrency}',
            ppcComponents        = {$ppcComponents|json_encode},
            ppcFundingDisabled   = '{$ppcFundingDisabled}',
            ppcCommit            = '{$ppcCommit}',
            ppcConsentID         = '{$ppcConsentID}',
            ppcConsentActive     = {$ppcConsentActive},
            ppcConsentGiven      = ppcConsentActive === false ? true : {$ppcConsentGiven},
            ppcBNCode            = '{$ppcBNCode}',
            wrapperLoaded        = false,
            loadedComponents     = [],
            reloadableComponents = ['productDetails', 'initProductDetailsECSButtons', 'initShippingSelectionButtons', 'orderProcess'];
        {literal}
        let config = {
            "client-id": ppcClientID,
            "currency" : ppcCurrency,
            "commit" : ppcCommit,
            "components": ppcComponents,
            "locale": ppcOrderLocale,
            "disable-funding": ppcFundingDisabled,
            "enable-funding" : 'paylater',
            "data-partner-attribution-id": ppcBNCode
        }

        if (ppcConsentGiven === true) {
            loadPaypalWrapper(config);
        }

        $(window).on('ppc:componentInit',function(event,initFunction,skipConsent) {
            if (skipConsent === true) {
                ppcConsentGiven = true;
            }
            if (wrapperLoaded === false) {
                checkConsent();
            }
        })
        $(window).on('ppc:requestConsent',function(event) {
            $(window).trigger('ppc:getConsent',ppcConsentGiven);
        })

        document.addEventListener('consent.updated', function (e) {
            if (e.detail[ppcConsentID]) {
                ppcConsentGiven = true;
                $(window).trigger('ppc:getConsent', ppcConsentGiven);
                loadPaypalWrapper(config);
            }
        });

        $(document).ready(function() {
            $(window).trigger('ppc:requestConsent');
        })

        function loadPaypalWrapper(config) {
            if (wrapperLoaded === false) {
                wrapperLoaded = true;
                window.paypalLoadScript(config).then((ppc_jtl) => {
                    runComponents(ppc_jtl);
                    $(window).off('ppc:componentInit').on('ppc:componentInit',function(event, initComponent) {
                        if (reloadableComponents.indexOf(initComponent.name) !== -1) {
                            loadedComponents.push(initComponent.name);
                            initComponent(ppc_jtl);
                        }
                        if (loadedComponents.indexOf(initComponent.name) === -1) {
                            loadedComponents.push(initComponent.name);
                            initComponent(ppc_jtl);
                        }
                    })
                }).catch( (err) => {

                });
            }
        }

        function checkConsent() {
            if (ppcConsentGiven === false) {
                if (typeof CM !== 'undefined') {
                    if (CM.getSettings(ppcConsentID) === false) {
                        CM.openConfirmationModal(ppcConsentID, function () {
                            ppcConsentGiven = true;
                            $(window).trigger('ppc:getConsent',ppcConsentGiven);
                            loadPaypalWrapper(config);
                        });
                    }
                }
            } else {
                loadPaypalWrapper(config);
            }
        }

        function runComponents(ppc_jtl) {
            if (typeof(window.PPCcomponentInitializations) !== 'undefined') {
                let components = window.PPCcomponentInitializations;
                for (let i in components) {
                    if (loadedComponents.indexOf(components[i].name) === -1) {
                        loadedComponents.push(components[i].name);
                        components[i](ppc_jtl);
                    }
                }
            }
        }
    {/literal}
</script>
