<script>
    if (typeof(window.PPCcomponentInitializations) === 'undefined') {
        window.PPCcomponentInitializations = [];
    }

    (function() {
        let methodID             = {$ppcPaymentMethodID},
            ppcMethodName        = '{$ppcMethodName}',
            ppcModuleID          = '{$ppcModuleID}',
            ppcOption            = '{$ppcFundingSource}',
            paymentFundsMapping  = JSON.parse('{$ppcFundingMethodsMapping}'),
            ppcActiveTemplate    = '{$ppcActiveTemplate}',
            ppcSingleFunding     = {if $ppcSingleFunding}true{else}false{/if},
            ppcPaymentResetTitle = ppcSingleFunding ? '{__('Zahlungsart ändern')}' : '',
            activePaymentMethod  = {if is_int($AktiveZahlungsart)}{$AktiveZahlungsart}{else}'{$AktiveZahlungsart}'{/if},
            initInProgress       = false;

        window.PPCcomponentInitializations.push(initShippingSelectionButtons);

        $(document).ready(function() {
            $(window).trigger('ppc:componentInit',[initShippingSelectionButtons, true]);
        });
        {literal}
        function initShippingSelectionButtons(ppc_jtl)
        {
            if (initInProgress) {
                return;
            }
            initInProgress = true;
            let initCallback = function () {
                let ppcPaymentContainer = $('#' + ppcModuleID);

                if (typeof ppc_jtl !== 'undefined') {
                    let ppcInserted = false;
                    let fundingSources;

                    try {
                        fundingSources = ppc_jtl.getFundingSources();
                    } catch (err) {
                        fundingSources = [];
                        $('.checkout-payment-method').removeClass('d-none');
                    }

                    let eligibleOptionIds = [];

                    fundingSources.sort(function (a, b) {
                        return b === 'paypal' ? -1 : 0;
                    }).forEach(function (fundingSource) {
                        if (typeof fundingSource === 'undefined') {
                            return;
                        }
                        let mark = ppc_jtl.Marks({fundingSource: fundingSource});

                        if (mark.isEligible() && (!ppcSingleFunding || ppcOption === fundingSource)) {
                            ppcInserted = true;
                            let paymentOptionId = 'za_ppc_' + fundingSource;
                            let fundingSourceTitle = paymentFundsMapping[fundingSource];
                            let template = paymentButtonsOptionTemplate({
                                paymentOptionId,
                                fundingSourceTitle,
                                ppcMethodName,
                                methodID,
                                fundingSource,
                                ppcActiveTemplate,
                                ppcPaymentResetTitle,
                            });
                            eligibleOptionIds.push(paymentOptionId);
                            ppcPaymentContainer.after(template);
                            mark.render('#' + paymentOptionId + '_img');
                            if (methodID === activePaymentMethod) {
                                ppcOption = ppcOption === '' ? sessionStorage.getItem('chosenPPCPaymentOption') : ppcOption;
                                if (ppcOption !== null && ppcOption !== '') {
                                    $('#za_ppc_' + ppcOption + '_input').prop('checked', true);
                                }
                            }
                        }
                    });

                    setTimeout(() => {
                        let widestWidth = 0;

                        eligibleOptionIds.forEach((eligibleOptionId) => {
                            let elWidth = $('#' + eligibleOptionId + '_img').width();
                            if (widestWidth < elWidth) {
                                widestWidth = elWidth;
                            }
                        });

                        $('.ppc-option-img').each(function () {
                            $(this).css('width', widestWidth);
                        });
                    }, 0);

                    if (ppcInserted) {
                        ppcPaymentContainer.remove();
                        $('.checkout-payment-method').removeClass('d-none');
                    }

                    setTimeout(function () {
                        $('input[type=radio][name=Zahlungsart]').change(function () {
                            let attr = $(this).attr('ppc-funding-source');
                            let attrIsSet = typeof (attr) !== 'undefined' && attr !== false;

                            ppcOption = $(this).is(':checked') && attrIsSet ? attr : '';
                        });
                    });
                    $('#fieldset-payment .jtl-spinner').fadeOut(300,
                        function () {
                            $(this).remove();
                        }
                    );
                } else {
                    $('.checkout-payment-method').removeClass('d-none');
                }
                $('.checkout-shipping-form').on('submit', function (e) {
                    if (parseInt($('input[name="Zahlungsart"]:checked', $(this)).val()) === methodID) {
                        sessionStorage.setItem('chosenPPCPaymentOption', ppcOption);
                        $('#ppc-funding-source_input').val(ppcOption);
                    } else {
                        $('#ppc-funding-source_input').val('');
                    }
                });
            }
            if (typeof paymentButtonsOptionTemplate === 'undefined') {
                let tplScript = document.createElement('script');
                tplScript.onload = function () {
                    window.setTimeout(initCallback, 100);
                }
                tplScript.src = "{/literal}{$ppcFrontendUrl}{literal}template/jsTemplates/paymentButtonsOption.js";
                document.getElementsByTagName( "head" )[0].appendChild(tplScript);
            } else {
                initCallback();
            }
        }
        {/literal}
    })();
</script>
