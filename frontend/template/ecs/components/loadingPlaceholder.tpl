<div class="d-none hidden" id="ppc-loading-placeholder-{$ppcNamespace}">
    <div class="spinner-border spinner-border-sm mr-2 " role="status">
        <span class="sr-only">Loading...</span>
    </div>
    <img class="ppc-paypal-button-custom-img" src="{$ppcFrontendUrl}img/paypal_color.svg" />
    <span class="ppc-loading-placeholder-text">{$ppcLoadingPlaceholder} ...</span>
</div>
