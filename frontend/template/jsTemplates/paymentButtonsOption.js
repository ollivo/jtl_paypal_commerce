const paymentButtonsOptionTemplate = ({paymentOptionId, fundingSourceTitle, ppcMethodName, methodID, fundingSource, ppcActiveTemplate, ppcPaymentResetTitle}) => {
    let resetButton = ppcPaymentResetTitle !== ''
        ? `<div class="custom-control custom-button custom-control-inline">
                <button class="btn btn-outline-secondary btn-block" name="resetPayment" value="1" type="submit">${ppcPaymentResetTitle}</button>
           </div>`
        : '';
    return `
        <div class="col checkout-payment-method col-12 ppc-checkout-payment-method" id="${paymentOptionId}">
            <div class="custom-control custom-radio custom-control-inline">
                <input ppc-funding-source="${fundingSource}" class="custom-control-input " type="radio" id="${paymentOptionId}_input" name="Zahlungsart" value="${methodID}"/>
                <label for="${paymentOptionId}_input" class="custom-control-label d-flex align-items-center">
                    <div id="${paymentOptionId}_img" class="align-items-center d-inline-block ppc-option-img"></div>
                    <div class="funding-name order-2 d-inline-block ml-1">
                        ${fundingSourceTitle}
                    </div>
                </label>
            </div>
            ${resetButton}
        </div>`
}
