<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\frontend;

use Exception;
use JTL\Alert\Alert;
use JTL\Cart\Cart;
use JTL\Catalog\Product\Artikel;
use JTL\Customer\Customer;
use JTL\Customer\CustomerGroup;
use JTL\Exceptions\CircularReferenceException;
use JTL\Exceptions\ServiceNotFoundException;
use JTL\Helpers\Request;
use JTL\Helpers\ShippingMethod;
use JTL\Link\LinkInterface;
use JTL\Plugin\PluginInterface;
use JTL\Session\Frontend;
use JTL\Shop;
use JTL\Smarty\JTLSmarty;
use Plugin\jtl_paypal_commerce\paymentmethod\Helper;
use Plugin\jtl_paypal_commerce\paymentmethod\InvalidPayerDataException;
use Plugin\jtl_paypal_commerce\paymentmethod\PayPalPaymentInterface;
use Plugin\jtl_paypal_commerce\PPC\Authorization\MerchantCredentials;
use Plugin\jtl_paypal_commerce\PPC\BackendUIsettings;
use Plugin\jtl_paypal_commerce\PPC\APM;
use Plugin\jtl_paypal_commerce\PPC\Configuration;
use Plugin\jtl_paypal_commerce\PPC\Environment\EnvironmentInterface;
use Plugin\jtl_paypal_commerce\PPC\Order\AppContext;
use Plugin\jtl_paypal_commerce\PPC\Order\OrderStatus;
use Plugin\jtl_paypal_commerce\PPC\Order\Transaction;
use stdClass;
use function Functional\first;

/**
 * Class CheckoutPage
 * @package Plugin\jtl_paypal_commerce\frontend
 */
final class CheckoutPage
{
    /** @var PluginInterface */
    private $plugin;

    /** @var Configuration */
    private $config;

    /** @var int */
    private $pageStep;


    /** @var self[] */
    public static $instance;

    public const STEP_UNKNOWN    = 0;
    public const STEP_SHIPPING   = 1;
    public const STEP_CONFIRM    = 2;
    public const PRODUCT_DETAILS = 3;
    public const CART            = 4;
    public const STEP_ADDRESS    = 5;

    public const PAGE_SCOPE_PRODUCTDETAILS = 'productDetails';
    public const PAGE_SCOPE_MINICART       = 'miniCart';
    public const PAGE_SCOPE_CART           = 'cart';
    public const PAGE_SCOPE_ORDERPROCESS   = 'orderProcess';
    public const PAGE_SCOPES               = [
        self::PAGE_SCOPE_PRODUCTDETAILS,
        self::PAGE_SCOPE_MINICART,
        self::PAGE_SCOPE_CART,
        self::PAGE_SCOPE_ORDERPROCESS,
    ];

    /**
     * CheckoutPage constructor.
     * @param PluginInterface    $plugin
     * @param Configuration|null $config
     */
    private function __construct(PluginInterface $plugin, Configuration $config = null)
    {
        $this->plugin = $plugin;
        $this->config = $config ?? Shop::Container()->get(Configuration::class);

        self::$instance[$plugin->getPluginID()] = $this;
    }

    /**
     * @param PluginInterface $plugin
     * @return self
     */
    public static function getInstance(PluginInterface $plugin): self
    {
        return self::$instance[$plugin->getPluginID()] ?? new self(
            $plugin,
            Shop::Container()->get(Configuration::class)
        );
    }

    /**
     * @return int
     */
    public function getPageStep(): int
    {
        return $this->pageStep ?? self::STEP_UNKNOWN;
    }

    /**
     * @param int $pageStep
     */
    public function setPageStep(int $pageStep): void
    {
        $this->pageStep = $pageStep;
    }

    /**
     * @param JTLSmarty $smarty
     * @return void
     */
    public function validatePayment(JTLSmarty $smarty): void
    {
        $payment = Helper::getInstance($this->plugin)->getPaymentFromName('PayPalCommerce');
        if ($payment === null) {
            return;
        }

        $ppOrder = $payment->getPPOrder();
        if ($ppOrder === null || $ppOrder->getStatus() !== OrderStatus::STATUS_APPROVED) {
            return;
        }

        // Reset current shipping methods to usable with this payment
        $customer        = Frontend::getCustomer();
        $shippingMethods = $smarty->getTemplateVars('Versandarten');
        foreach (\array_keys($shippingMethods) as $key) {
            if (!$payment->isAssigned(
                ShippingMethod::getShippingClasses(Frontend::getCart()),
                $customer->getID() > 0
                    ? $customer->getID()
                    : CustomerGroup::getDefaultGroupID(),
                $shippingMethods[$key]->kVersandart
            )) {
                unset($shippingMethods[$key]);
            }
        }
        $smarty->assign('Versandarten', $shippingMethods);

        // Reset current payment methods to only this payment
        $paymentMethods = $smarty->getTemplateVars('Zahlungsarten');
        foreach (\array_keys($paymentMethods) as $key) {
            if ($payment->getMethod()->getMethodID() !== $paymentMethods[$key]->kZahlungsart) {
                unset($paymentMethods[$key]);
            }
        }
        $smarty->assign('Zahlungsarten', $paymentMethods);
    }

    /**
     * @param JTLSmarty $smarty
     * @return void
     * @throws CircularReferenceException
     * @throws ServiceNotFoundException
     */
    public function render(JTLSmarty $smarty): void
    {
        $ppcpPayment = Helper::getInstance($this->plugin)->getPaymentFromName('PayPalCommerce');
        $ppPUI       = Helper::getInstance($this->plugin)->getPaymentFromName('PayPalPUI');
        $customer    = Frontend::getCustomer();
        $cart        = Frontend::getCart();

        try {
            switch ($this->getPageStep()) {
                case self::STEP_SHIPPING:
                    if ($ppcpPayment !== null && $ppcpPayment->isValid($customer, $cart)) {
                        $this->shippingPPCPayment($ppcpPayment, $smarty);
                    }

                    if ($ppPUI !== null && $ppPUI->isValid($customer, $cart)) {
                        $this->shippingPUI($ppPUI, $smarty);
                    }

                    break;
                case self::STEP_ADDRESS:
                    if (!$ppcpPayment->isValid($customer, $cart)) {
                        return;
                    }
                    $components = [];
                    $this->preloadECSJS(self::PAGE_SCOPE_ORDERPROCESS);
                    if ($ppcpPayment->isValidExpressPayment($customer, $cart)
                        && $this->renderOrderProcessButtons($smarty)
                    ) {
                        $components[] = BackendUIsettings::COMPONENT_BUTTONS;
                        $components[] = BackendUIsettings::COMPONENT_FUNDING_ELIGIBILITY;
                    }
                    $this->renderPayPalJsSDK($ppcpPayment, $smarty, $components, false, true);

                    break;
                case self::STEP_CONFIRM:
                    $paymentId = (int)(Frontend::get('Zahlungsart')->kZahlungsart ?? 0);
                    if ($ppcpPayment->getMethod()->getMethodID() === $paymentId) {
                        $this->confirmPPCPayment($ppcpPayment, $customer, $cart, $smarty);
                    } else {
                        $ppcpPayment->unsetCache();
                    }

                    if ($ppPUI->getMethod()->getMethodID() === $paymentId) {
                        $this->confirmPUI($ppPUI, $customer, $cart, $smarty);
                    } else {
                        $ppPUI->unsetCache();
                    }

                    break;
                case self::PRODUCT_DETAILS:
                    if (!$ppcpPayment->isValid($customer, $cart)) {
                        return;
                    }
                    $components = [];
                    $this->preloadInstalmentBannerJS(self::PAGE_SCOPE_PRODUCTDETAILS);
                    $this->preloadECSJS(self::PAGE_SCOPE_PRODUCTDETAILS);

                    if ($this->renderInstalmentBanner($smarty, self::PAGE_SCOPE_PRODUCTDETAILS)) {
                        $components[] = BackendUIsettings::COMPONENT_MESSAGES;
                    }
                    if ($ppcpPayment->isValidExpressPayment($customer, $cart)) {
                        if ($this->renderProductDetailsButtons($smarty)) {
                            $components[] = BackendUIsettings::COMPONENT_BUTTONS;
                            $components[] = BackendUIsettings::COMPONENT_FUNDING_ELIGIBILITY;
                        }

                        $components = $this->renderMiniCartComponents(
                            $smarty,
                            $ppcpPayment,
                            $customer,
                            $cart,
                            $components
                        );
                    }

                    $this->renderPayPalJsSDK($ppcpPayment, $smarty, $components, false, true);

                    break;
                case self::CART:
                    if (!$ppcpPayment->isValid($customer, $cart)) {
                        return;
                    }
                    $components = [];
                    $this->preloadInstalmentBannerJS(self::PAGE_SCOPE_CART);
                    $this->preloadECSJS(self::PAGE_SCOPE_CART);

                    if ($this->renderInstalmentBanner(
                        $smarty,
                        self::PAGE_SCOPE_CART
                    )) {
                        $components[] = BackendUIsettings::COMPONENT_MESSAGES;
                    }
                    if ($ppcpPayment->isValidExpressPayment($customer, $cart) && $this->renderCartButtons($smarty)) {
                        $components[] = BackendUIsettings::COMPONENT_BUTTONS;
                        $components[] = BackendUIsettings::COMPONENT_FUNDING_ELIGIBILITY;
                    }

                    $components = $this->renderMiniCartComponents($smarty, $ppcpPayment, $customer, $cart, $components);

                    $this->renderPayPalJsSDK($ppcpPayment, $smarty, $components, false, true);

                    break;
            }
        } catch (Exception $e) {
            $logger = Shop::Container()->getLogService();
            $logger->addRecord($logger::ERROR, 'page can not be rendered (' . $e->getMessage() . ')');
        }
    }

    /**
     * @param PayPalPaymentInterface $ppcpPayment
     * @param JTLSmarty              $smarty
     * @throws CircularReferenceException
     * @throws ServiceNotFoundException
     */
    private function shippingPPCPayment(
        PayPalPaymentInterface $ppcpPayment,
        JTLSmarty $smarty
    ): void {
        Transaction::instance()->clearAllTransactions();
        $components = [
            BackendUIsettings::COMPONENT_BUTTONS,
            BackendUIsettings::COMPONENT_FUNDING_ELIGIBILITY,
            BackendUIsettings::COMPONENT_MARKS
        ];
        $this->preloadInstalmentBannerJS(self::PAGE_SCOPE_PRODUCTDETAILS);
        $this->renderPaymentButtons($ppcpPayment, $smarty);

        if ($this->renderInstalmentBanner(
            $smarty,
            self::PAGE_SCOPE_ORDERPROCESS
        )) {
            $components[] = BackendUIsettings::COMPONENT_MESSAGES;
        }

        $this->renderPayPalJsSDK($ppcpPayment, $smarty, $components);
    }

    /**
     * @param PayPalPaymentInterface $ppPUI
     * @param JTLSmarty              $smarty
     * @throws CircularReferenceException
     * @throws ServiceNotFoundException
     */
    public function shippingPUI(PayPalPaymentInterface $ppPUI, JTLSmarty $smarty): void
    {
        Transaction::instance()->clearAllTransactions();
        $puiMethod = $ppPUI->getMethod();
        try {
            \pq('#' . $puiMethod->getModuleID())
                ->append($smarty
                    ->assign('puiPaymentId', $puiMethod->getMethodID())
                    ->assign(
                        'legalInformation',
                        $this->plugin->getLocalization()->getTranslation('jtl_paypal_pui_legalinformation')
                    )
                    ->fetch($puiMethod->getAdditionalTemplate()));
        } catch (Exception $e) {
            $logger = Shop::Container()->getLogService();
            $logger->addRecord(
                $logger::ERROR,
                'phpquery rendering failed: shippingPUI()'
            );

            return;
        }
    }

    /**
     * @param PayPalPaymentInterface $ppcpPayment
     * @param Customer               $customer
     * @param Cart                   $cart
     * @param JTLSmarty              $smarty
     * @throws CircularReferenceException
     * @throws ServiceNotFoundException
     */
    private function confirmPPCPayment(
        PayPalPaymentInterface $ppcpPayment,
        Customer $customer,
        Cart $cart,
        JTLSmarty $smarty
    ): void {
        $fundingSource = Request::postVar('ppc-funding-source', '');
        if ($fundingSource === '' && Request::verifyGPCDataInt('wk') === 1) {
            // direct call from 1-click-checkout has no founding source and is not allowed
            Helper::redirectAndExit($ppcpPayment->getPaymentCancelURL());
            exit();
        }

        $ppcOrderId = $ppcpPayment->createPPOrder(
            $customer,
            $cart,
            $fundingSource,
            AppContext::SHIPPING_PROVIDED,
            AppContext::PAY_NOW,
            $ppcpPayment->getCache('BNCode') ?? MerchantCredentials::BNCODE_CHECKOUT
        );
        if ($ppcOrderId === null) {
            // ToDo: Frontend error message
            Helper::redirectAndExit($ppcpPayment->getPaymentCancelURL());
            exit();
        }
        $this->renderOrderConfirmationButtons($ppcpPayment, $ppcOrderId, $smarty);
        $this->renderPayPalJsSDK($ppcpPayment, $smarty, [BackendUIsettings::COMPONENT_BUTTONS]);
    }

    /**
     * @param PayPalPaymentInterface $ppPUI
     * @param Customer               $customer
     * @param Cart                   $cart
     * @param JTLSmarty              $smarty
     * @throws CircularReferenceException
     * @throws ServiceNotFoundException
     */
    private function confirmPUI(
        PayPalPaymentInterface $ppPUI,
        Customer $customer,
        Cart $cart,
        JTLSmarty $smarty
    ): void {
        try {
            $ppPUI->validatePayerData($customer, Frontend::getDeliveryAddress(), $cart);
            $this->renderPUIConfirmation($ppPUI, $smarty);
        } catch (InvalidPayerDataException $e) {
            $alerts = Shop::Container()->getAlertService();
            if (!$e->hasAlerts()) {
                $alerts->addAlert(Alert::TYPE_ERROR, $e->getMessage(), 'confirmPUI', [
                    'saveInSession' => true,
                ]);
            } else {
                foreach ($e->getAlerts() as $alert) {
                    $alerts->removeAlertByKey($alert->getKey());
                    $alerts->getAlertlist()->push($alert);
                }
            }

            if ($e->hasRedirectURL()) {
                Helper::redirectAndExit($e->getRedirectURL());
                exit();
            }
        }
    }

    /**
     * @param PayPalPaymentInterface $payment
     * @param string                 $ppcOrderId
     * @param JTLSmarty              $smarty
     * @noinspection JsonEncodingApiUsageInspection
     */
    private function renderOrderConfirmationButtons(
        PayPalPaymentInterface $payment,
        string $ppcOrderId,
        JTLSmarty $smarty
    ): void {
        $curMethod = Frontend::get('Zahlungsart');
        if ((int)$curMethod->kZahlungsart !== $payment->getMethod()->getMethodID()) {
            return;
        }

        $ppOrder = $payment->getPPOrder($ppcOrderId);
        if ($ppOrder === null) {
            // ToDo: Frontend error message
            Helper::redirectAndExit($payment->getPaymentCancelURL());
            exit();
        }

        if ($ppOrder->getStatus() === OrderStatus::STATUS_APPROVED) {
            // Order is approved (from express checkout?) -  no call to paypal necessary
            try {
                \pq('#complete-order-button')
                    ->after($smarty
                        ->assign('ppcStateURL', $payment->getPaymentStateURL() ?? '')
                        ->fetch($this->plugin->getPaths()->getFrontendPath() . 'template/orderConfirmationSimple.tpl'));
            } catch (Exception $e) {
            }

            return;
        }

        $templateConfig    = $this->config->mapFrontendSettings(BackendUIsettings::BACKEND_SETTINGS_SECTION_GENERAL);
        $ppcActiveTemplate = $templateConfig['templateSupport'];

        try {
            \pq('body')->prepend(
                "<script src='" .
                $this->plugin->getPaths()->getFrontendURL() .
                "template/ecs/init.js'></script>"
            );
            $this->ECSDefaultSmartyAssignments($smarty);
            \pq('#complete-order-button')
                ->after($smarty
                    ->assign('ppcConfig', \json_encode($this->config->mapFrontendSettings()))
                    ->assign('ppcOrderId', $ppcOrderId)
                    ->assign('ppcActiveTemplate', $ppcActiveTemplate)
                    ->assign('ppcStateURL', $payment->getPaymentStateURL() ?? '#')
                    ->assign('ppcCancelURL', $payment->getPaymentCancelURL())
                    ->assign('ppcFundingSource', $payment->getFundingSource())
                    ->fetch($this->plugin->getPaths()->getFrontendPath() . 'template/orderConfirmation.tpl'));
        } catch (Exception $e) {
            return;
        }
    }

    /**
     * @param JTLSmarty $smarty
     * @return bool
     */
    private function renderProductDetailsButtons(JTLSmarty $smarty): bool
    {
        $config         = $this->config->mapFrontendSettings(null, null, [
            BackendUIsettings::BACKEND_SETTINGS_SECTION_SMARTPAYMENTBTNS,
            BackendUIsettings::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY
        ]);
        $expressBuyConf = $config[BackendUIsettings::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY];

        if (!$this->config->checkComponentVisibility($expressBuyConf, self::PAGE_SCOPE_PRODUCTDETAILS)) {
            return false;
        }

        $method   = $expressBuyConf[self::PAGE_SCOPE_PRODUCTDETAILS . '_phpqMethod'];
        $selector = $expressBuyConf[self::PAGE_SCOPE_PRODUCTDETAILS . '_phpqSelector'];
        /** @var Artikel $curProduct */
        $curProduct = Shop::Smarty()->getTemplateVars('Artikel');

        if ($curProduct === null || $curProduct->bHasKonfig || $curProduct->inWarenkorbLegbar <= 0) {
            return false;
        }

        try {
            $this->ECSDefaultSmartyAssignments($smarty);
            \pq($selector)->{$method}($smarty
                ->assign('ppcNamespace', self::PAGE_SCOPE_PRODUCTDETAILS)
                ->assign('ppcConfig', $config[BackendUIsettings::BACKEND_SETTINGS_SECTION_SMARTPAYMENTBTNS])
                ->assign('ecs_wk_error_desc', $this->plugin->getLocalization()->getTranslation(
                    'jtl_paypal_commerce_ecs_wk_error_desc'
                ))
                ->assign('ecs_wk_error_title', $this->plugin->getLocalization()->getTranslation(
                    'jtl_paypal_commerce_ecs_wk_error_title'
                ))
                ->fetch($this->plugin->getPaths()->getFrontendPath() . 'template/ecs/productDetails.tpl'));
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param JTLSmarty              $smarty
     * @return bool
     */
    public function renderMiniCartButtons(
        JTLSmarty $smarty
    ): bool {
        $config         = $this->config->mapFrontendSettings(null, null, [
            BackendUIsettings::BACKEND_SETTINGS_SECTION_SMARTPAYMENTBTNS,
            BackendUIsettings::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY
        ]);
        $expressBuyConf = $config[BackendUIsettings::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY];

        if (!$this->config->checkComponentVisibility($expressBuyConf, self::PAGE_SCOPE_MINICART)) {
            return false;
        }

        $method   = $expressBuyConf[self::PAGE_SCOPE_MINICART . '_phpqMethod'];
        $selector = $expressBuyConf[self::PAGE_SCOPE_MINICART . '_phpqSelector'];
        $tplPath  = 'template/ecs/miniCart.tpl';
        try {
            $this->ECSDefaultSmartyAssignments($smarty);
            \pq($selector)
                ->{$method}($smarty
                    ->assign('ppcNamespace', self::PAGE_SCOPE_MINICART)
                    ->assign('ppcConfig', $config[BackendUIsettings::BACKEND_SETTINGS_SECTION_SMARTPAYMENTBTNS])
                    ->assign('ppcPrice', Frontend::getCart()->gibGesamtsummeWaren(
                        !Frontend::getCustomerGroup()->isMerchant()
                    ))
                    ->fetch($this->plugin->getPaths()->getFrontendPath() . $tplPath));
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param JTLSmarty              $smarty
     * @param PayPalPaymentInterface $payment
     * @param Customer               $customer
     * @param Cart                   $cart
     * @param array                  $components
     * @return array
     */
    public function renderMiniCartComponents(
        JTLSmarty $smarty,
        PayPalPaymentInterface $payment,
        Customer $customer,
        Cart $cart,
        array $components = []
    ): array {
        if ($payment->isValidExpressPayment($customer, $cart) && $this->renderMiniCartButtons($smarty)) {
            $components[] = BackendUIsettings::COMPONENT_BUTTONS;
            $components[] = BackendUIsettings::COMPONENT_FUNDING_ELIGIBILITY;
        }
        if ($this->renderInstalmentBanner($smarty, self::PAGE_SCOPE_MINICART)) {
            $components[] = BackendUIsettings::COMPONENT_MESSAGES;
        }

        return $components;
    }

    /**
     * @param JTLSmarty $smarty
     * @return bool
     */
    private function renderOrderProcessButtons(JTLSmarty $smarty): bool
    {
        $config         = $this->config->mapFrontendSettings(null, null, [
            BackendUIsettings::BACKEND_SETTINGS_SECTION_SMARTPAYMENTBTNS,
            BackendUIsettings::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY
        ]);
        $expressBuyConf = $config[BackendUIsettings::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY];

        if (!$this->config->checkComponentVisibility($expressBuyConf, self::PAGE_SCOPE_ORDERPROCESS)) {
            return false;
        }

        $method   = $expressBuyConf[self::PAGE_SCOPE_ORDERPROCESS . '_phpqMethod'];
        $selector = $expressBuyConf[self::PAGE_SCOPE_ORDERPROCESS . '_phpqSelector'];

        try {
            $this->ECSDefaultSmartyAssignments($smarty);
            \pq($selector)
                ->{$method}($smarty
                    ->assign('ppcNamespace', self::PAGE_SCOPE_ORDERPROCESS)
                    ->assign('ppcConfig', $config[BackendUIsettings::BACKEND_SETTINGS_SECTION_SMARTPAYMENTBTNS])
                    ->assign('ppcPrice', Frontend::getCart()->gibGesamtsummeWaren(
                        !Frontend::getCustomerGroup()->isMerchant()
                    ))
                    ->fetch($this->plugin->getPaths()->getFrontendPath() . 'template/ecs/orderProcess.tpl'));
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param PayPalPaymentInterface $payment
     * @param JTLSmarty              $smarty
     * @return void
     * @throws CircularReferenceException
     * @throws ServiceNotFoundException
     */
    private function renderPUIConfirmation(PayPalPaymentInterface $payment, JTLSmarty $smarty): void
    {
        $merchantID = $this->config->getPrefixedConfigItem('merchantID_' . $this->config->getWorkingMode());
        /** @var EnvironmentInterface $environment */
        $environment = Shop::Container()->get(EnvironmentInterface::class);

        try {
            \pq('body')
                ->append($smarty
                    ->assign('fraudnetGUID', $environment->getMetaDataId())
                    ->assign('fraudnetPageID', $merchantID . '_checkout-page')
                    ->assign('ppcStateURL', $payment->getPaymentStateURL() ?? '')
                    ->assign('isSandbox', $environment->isSandbox())
                    ->fetch($this->plugin->getPaths()->getFrontendPath() . 'template/paypalFraudnet.tpl'));
        } catch (Exception $e) {
            $logger = Shop::Container()->getLogService();
            $logger->addRecord(
                $logger::ERROR,
                'phpquery rendering failed: renderPUIConfirmation()'
            );

            return;
        }
    }

    /**
     * @param JTLSmarty $smarty
     */
    private function ECSDefaultSmartyAssignments(JTLSmarty $smarty) : void
    {
        /** @var LinkInterface $ecsLink */
        $ecsLink           = $this->plugin->getLinks()->getLinks()->first(static function (LinkInterface $link) {
            return $link->getTemplate() === 'expresscheckout.tpl';
        });
        $templateConfig    = $this->config->mapFrontendSettings(BackendUIsettings::BACKEND_SETTINGS_SECTION_GENERAL);
        $ppcActiveTemplate = $templateConfig['templateSupport'];

        $smarty
            ->assign('ppcFrontendUrl', $this->plugin->getPaths()->getFrontendURL())
            ->assign('ppcClientID', $this->config->getClientID())
            ->assign('ppcActiveTemplate', $ppcActiveTemplate)
            ->assign('ppcECSUrl', $ecsLink !== null ? $ecsLink->getURL() : '')
            ->assign(
                'ppcPreloadButtonLabelInactive',
                $this->plugin->getLocalization()->getTranslation(
                    'jtl_paypal_commerce_ecs_preload_button_label_inactive'
                )
            )
            ->assign(
                'ppcLoadingPlaceholder',
                $this->plugin->getLocalization()->getTranslation(
                    'jtl_paypal_commerce_ecs_loading_placeholder'
                )
            )
            ->assign(
                'ppcPreloadButtonLabelActive',
                $this->plugin->getLocalization()->getTranslation(
                    'jtl_paypal_commerce_ecs_preload_button_label_active'
                )
            );
    }

    /**
     * @param JTLSmarty $smarty
     * @return bool
     */
    private function renderCartButtons(JTLSmarty $smarty): bool
    {
        $config         = $this->config->mapFrontendSettings(null, null, [
            BackendUIsettings::BACKEND_SETTINGS_SECTION_SMARTPAYMENTBTNS,
            BackendUIsettings::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY
        ]);
        $expressBuyConf = $config[BackendUIsettings::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY];

        if (!$this->config->checkComponentVisibility($expressBuyConf, self::PAGE_SCOPE_CART)) {
            return false;
        }

        $method   = $expressBuyConf[self::PAGE_SCOPE_CART . '_phpqMethod'];
        $selector = $expressBuyConf[self::PAGE_SCOPE_CART . '_phpqSelector'];

        try {
            $this->ECSDefaultSmartyAssignments($smarty);
            \pq($selector)->{$method}($smarty
                    ->assign('ppcNamespace', self::PAGE_SCOPE_CART)
                    ->assign('ppcConfig', $config[BackendUIsettings::BACKEND_SETTINGS_SECTION_SMARTPAYMENTBTNS])
                    ->fetch($this->plugin->getPaths()->getFrontendPath() . 'template/ecs/cart.tpl'));
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param PayPalPaymentInterface $payment
     * @param JTLSmarty              $smarty
     * @noinspection JsonEncodingApiUsageInspection
     */
    private function renderPaymentButtons(PayPalPaymentInterface $payment, JTLSmarty $smarty): void
    {
        $methods  = $smarty->getTemplateVars('Zahlungsarten');
        $methodID = $payment->getMethod()->getMethodID();
        $method   = first($methods, static function (stdClass $method) use ($methodID) {
            return $method->kZahlungsart === $methodID;
        });
        $ppOrder  = $payment->getPPOrder();

        if ($method === null) {
            return;
        }

        $ppcFundingMethodsMapping = \json_encode(
            Helper::getInstance($this->plugin)->getFundingMethodsMapping(Shop::getLanguageCode())->toArray()
        );
        try {
            //Hide all preloaded payment methods before SmartPaymentButtons are rendered
            \pq('.checkout-payment-method')->addClass('d-none');
            \pq('.checkout-shipping-form')->append(
                '<input id="ppc-funding-source_input" type="hidden" name="ppc-funding-source" '
                . 'value="' . $payment->getFundingSource() . '">'
            );
            \pq('#fieldset-payment')->append('<div class="jtl-spinner"><i class="fa fa-spinner fa-pulse"></i></div>');
            \pq('#result-wrapper')->append(
                $smarty
                    ->assign('ppcFundingMethodsMapping', $ppcFundingMethodsMapping)
                    ->assign('ppcModuleID', $payment->getMethod()->getModuleID())
                    ->assign('ppcMethodName', $payment->getMethod()->getName())
                    ->assign('ppcFrontendUrl', $this->plugin->getPaths()->getFrontendURL())
                    ->assign('ppcPaymentMethodID', $payment->getMethod()->getMethodID())
                    ->assign('ppcFundingSource', $payment->getFundingSource())
                    ->assign('ppcSingleFunding', $ppOrder !== null
                        && $ppOrder->getStatus() === OrderStatus::STATUS_APPROVED)
                    ->assign(
                        'ppcActiveTemplate',
                        Shop::Container()->getTemplateService()->getActiveTemplate()->getName()
                    )
                    ->fetch($this->plugin->getPaths()->getFrontendPath() . 'template/paymentButtons.tpl')
            );
        } catch (Exception $e) {
            // ToDo: Error handling
        }
    }

    /**
     * @param JTLSmarty $smarty
     * @param string    $page
     * @return bool
     */
    public function renderInstalmentBanner(
        JTLSmarty $smarty,
        string $page
    ): bool {
        $mappedConfigs = $this->config->mapFrontendSettings(null, null, [
            BackendUIsettings::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP,
            BackendUIsettings::BACKEND_SETTINGS_SECTION_GENERAL
        ]);
        $config        = $mappedConfigs[BackendUIsettings::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP];
        try {
            $amount = $page === self::PAGE_SCOPE_PRODUCTDETAILS ?
                \pq('meta[itemprop="price"]')->attr('content') :
                Frontend::getCart()->gibGesamtsummeWaren(!Frontend::getCustomerGroup()->isMerchant());
        } catch (Exception $e) {
            return false;
        }

        if (!$this->config->checkComponentVisibility($config, $page) || $amount < $config[$page . '_minAmount']) {
            return false;
        }

        $method   = $config[$page . '_phpqMethod'];
        $selector = $config[$page . '_phpqSelector'];

        $ppcFrontendPath = $this->plugin->getPaths()->getFrontendPath();

        $ppcStyle = [
            'placement' => 'product',
            'amount' => $amount,
            'style' => [
                'layout' => $config[$page . '_layout'],
                'logo' => [
                    'type' => $config[$page . '_logoType']
                ],
                'text' => [
                    'size' => $config[$page . '_textSize'],
                    'color' => $config[$page . '_textColor']
                ],
                'color' => $config[$page . '_layoutType'],
                'ratio' => $config[$page . '_layoutRatio']

            ]
        ];
        $ppcActiveTemplate = $mappedConfigs[BackendUIsettings::BACKEND_SETTINGS_SECTION_GENERAL]['templateSupport'];
        try {
            \pq($selector)->{$method}($smarty
                ->assign('ppcStyle', $ppcStyle)
                ->assign('ppcFrontendUrl', $this->plugin->getPaths()->getFrontendURL())
                ->assign('ppcFrontendPath', $ppcFrontendPath)
                ->assign('ppcConsentPlaceholder', $this->plugin->getLocalization()->getTranslation(
                    'jtl_paypal_commerce_instalment_banner_consent_placeholder'
                ))
                ->assign('ppcComponentName', $page)
                ->assign('ppcActiveTemplate', $ppcActiveTemplate)
                ->fetch($ppcFrontendPath . 'template/instalmentBanner/' . $page . '/banner.tpl'));
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param string $page
     * @return bool
     */
    public function preloadInstalmentBannerJS(string $page = self::PAGE_SCOPE_MINICART): bool
    {
        $config = $this->config->mapFrontendSettings(BackendUIsettings::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP);

        if (!$this->config->checkComponentVisibility($config, $page) &&
            !$this->config->checkComponentVisibility($config, self::PAGE_SCOPE_MINICART)
        ) {
            return false;
        }
        try {
            \pq('body')->prepend(
                "<script src='" .
                $this->plugin->getPaths()->getFrontendURL() .
                "template/instalmentBanner/jsTemplates/instalmentBannerPlaceholder.js'></script>"
            );
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param string $page
     * @return bool
     */
    public function preloadECSJS(string $page): bool
    {
        $config = $this->config->mapFrontendSettings(BackendUIsettings::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY);

        if (!$this->config->checkComponentVisibility($config, $page) &&
            !$this->config->checkComponentVisibility($config, self::PAGE_SCOPE_MINICART)
        ) {
            return false;
        }
        try {
            \pq('body')->prepend(
                "<script src='" .
                $this->plugin->getPaths()->getFrontendURL() .
                "template/ecs/jsTemplates/standaloneButtonTemplate.js'></script>
                <script src='" .
                $this->plugin->getPaths()->getFrontendURL() .
                "template/ecs/jsTemplates/activeButtonLabelTemplate.js'></script>
                <script src='" .
                $this->plugin->getPaths()->getFrontendURL() .
                "template/ecs/init.js'></script>"
            );
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param PayPalPaymentInterface $ppcpPayment
     * @param JTLSmarty              $smarty
     * @param array                  $components
     * @param bool                   $ppcCommit
     * @param bool                   $isECS
     * @throws CircularReferenceException | ServiceNotFoundException
     */
    public function renderPayPalJsSDK(
        PayPalPaymentInterface $ppcpPayment,
        JTLSmarty $smarty,
        array $components,
        bool $ppcCommit = true,
        bool $isECS = false
    ): void {
        $components = \array_values(\array_unique($components));
        if (count($components) === 0) {
            return;
        }

        $locale            = Frontend::getInstance()->getLanguage()->cISOSprache === 'ger' ? 'de_DE' : 'en_GB';
        $cmActive          = Shop::getSettingValue(\CONF_CONSENTMANAGER, 'consent_manager_active') ?? 'N';
        $ppcConsentActive  = $cmActive === 'Y' && $this->config->getPrefixedConfigItem(
            BackendUIsettings::BACKEND_SETTINGS_SECTION_CONSENTMANAGER . '_activate'
        ) === 'Y';
        $ppcConsentGiven   = Shop::Container()->getConsentManager()->hasConsent(Configuration::CONSENT_ID);
        $config            = $this->config->mapFrontendSettings(BackendUIsettings::BACKEND_SETTINGS_SECTION_GENERAL);
        $ppcActiveTemplate = $config['templateSupport'];
        $APMs              = new APM($this->config);
        try {
            \pq('body')
                ->append($smarty
                    ->assign('ppcComponents', $components)
                    ->assign('ppcFrontendUrl', $this->plugin->getPaths()->getFrontendURL())
                    ->assign('ppcCommit', $ppcCommit ? 'true' : 'false')
                    ->assign('ppcFundingDisabled', \implode(',', $APMs->getDisabled($isECS)))
                    ->assign('ppcOrderLocale', $locale)
                    ->assign('ppcClientID', $this->config->getClientID())
                    ->assign('ppcConsentID', Configuration::CONSENT_ID)
                    ->assign('ppcConsentActive', $ppcConsentActive ? 'true' : 'false')
                    ->assign('ppcCurrency', Frontend::getCurrency()->getCode())
                    ->assign('ppcConsentGiven', $ppcConsentGiven ? 'true' : 'false')
                    ->assign('ppcActiveTemplate', $ppcActiveTemplate)
                    ->assign('ppcBNCode', $ppcpPayment->getCache('BNCode') ?? MerchantCredentials::BNCODE_CHECKOUT)
                    ->fetch($this->plugin->getPaths()->getFrontendPath() . 'template/paypalJsSDK.tpl'));
        } catch (Exception $e) {
            $logger = Shop::Container()->getLogService();
            $logger->addRecord(
                $logger::ERROR,
                'phpquery rendering failed: renderPayPalJsSDK()'
            );

            return;
        }
    }
}
